var express = require('express');
var router = express.Router();
const NcmController = require('../controllers/NcmController');

router.get('/:id', NcmController.find);
router.get('/', NcmController.list);

module.exports = router;