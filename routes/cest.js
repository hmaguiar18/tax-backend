var express = require('express');
var router = express.Router();
const CestController = require('../controllers/CestController');

router.post('/', CestController.store);

router.get('/', CestController.read);

router.get('/find', CestController.find);

  
module.exports = router;