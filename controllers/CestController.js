const Cest = require("../database/models/Cest");

module.exports = {
    async find(req, res) {
        const _codigoCest = req.body.codigo;

        try{
            const objCest = await Cest.findAll({
                where: {
                    codigoCest: _codigoCest
                }
            })
    
            return res.json(objCest);
        }
        catch (error) {
            res.status(400);
        }
    },

    async store(req, res) {
        const { codigoCest, descricao, opInterna } = req.body;

        const cest = await Cest.create({codigoCest, descricao, opInterna});

        return res.json(cest);
    },

    async read(req, res) {
    
        const codigosCest = await Cest.findAll();

        return res.json(codigosCest);
    }
};