const Ncm = require("../database/models/Ncm");

module.exports = {
    async find(req, res) {
        const _codigoNcm = req.params.id;

        try{
            const objNcm = await Ncm.findAll({
                where: {
                    codigoNcm: _codigoNcm
                }
            })
    
            res.header("Access-Control-Allow-Origin", "*");

            return res.json(objNcm);
        }
        catch (error) {
            res.status(400);
        }
    },

    async list(req, res) {
    
        const codigosNcm = await Ncm.findAll();

        return res.json(codigosNcm);
    },
};