'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('Ncm', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      codigoNcm: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      descricao: {
        type: Sequelize.STRING,
      }    
  });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('Ncm');
  }
};
