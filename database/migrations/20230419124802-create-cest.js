'use strict';

const { QueryInterface } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Cest', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        codigoCest: {
          type: Sequelize.STRING(10),
          allowNull: false,
        },
        descricao: {
          type: Sequelize.STRING,
        },
        opInterna: {
          type: Sequelize.TEXT('tiny'),
        }
      
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Cest');
  }
};
