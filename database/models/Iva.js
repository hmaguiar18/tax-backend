const { Model, DataTypes } = require('sequelize');

class Iva extends Model {
    static init(sequelize) {
        super.init({
              ordem: DataTypes.INTEGER,
              percentual: DataTypes.DECIMAL,
              especificacao: DataTypes.STRING(50),
              observacao: DataTypes.STRING(100),
              cestId: {type: DataTypes.INTEGER, foreignKey: true},
        },{sequelize})
    }
}

Iva.associations = (models) => {
    Iva.belongsTo(models.Cest, {foreignKey: 'id', as: 'Cest'});
};


module.exports = Iva;