const { Model, DataTypes } = require('sequelize');

class Cest extends Model {
    static init(sequelize) {
        super.init({
              codigoCest: DataTypes.STRING(10),
              descricao: DataTypes.STRING,
              opInterna: DataTypes.TEXT('tiny'),
        },{sequelize})
    }
};

Cest.associations = (models) => {
    Cest.hasMany(models.Iva, {foreignKey: 'id', as: 'Iva'});
};

module.exports = Cest;