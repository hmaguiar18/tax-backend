const { Model, DataTypes } = require('sequelize');

class Ncm extends Model {
    static init(sequelize) {
        super.init({
              codigoNcm: DataTypes.STRING(10),
              descricao: DataTypes.STRING,
        },{sequelize})
    }
}

module.exports = Ncm;