const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const Cest = require('./models/Cest');
const Ncm = require('./models/Ncm');

const connection = new Sequelize(dbConfig);

Cest.init(connection);
Ncm.init(connection);

module.exports = connection;

